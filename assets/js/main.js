function FazerRequisicao(parametros = {
    rota: '',
    todos,
    por_pagina,
    pagina,
    local_atividade,
    status_atividade,
    id_contratacao,
    responsavel_contratacao,
    divisao_contrato,
    status_contrato,
    responsavel_contrato,
    tipo_contrato,
    doc_contratual_contrato,
    situacao_fornecedor,
    porte_fornecedor,
    ramo_fornecedor,
    divisao_aeic,
    status_aeic,
    responsavel_aeic,
    tipo_aeic,
    doc_contratual_aeic
}) {
    const {
        rota = '',
        todos,
        por_pagina,
        pagina,
        local_atividade,
        status_atividade,
        id_contratacao,
        responsavel_contratacao,
        divisao_contrato,
        status_contrato,
        responsavel_contrato,
        tipo_contrato,
        doc_contratual_contrato,
        situacao_fornecedor,
        porte_fornecedor,
        ramo_fornecedor,
        divisao_aeic,
        status_aeic,
        responsavel_aeic,
        tipo_aeic,
        doc_contratual_aeic
    } = parametros;

    const linkRota = document.querySelector(".rota-de-chamada");
    const jsonContainer = document.getElementById("json-container");
    const filename = document.getElementById("filename");
    const elementoContent = document.getElementById("content");
    const elementoRota = document.getElementById("ferramentas");
    const elementoShare = document.getElementById("share");
    const elementoShareArea = document.getElementById("area-de-compartilhamento");

    const itemsPorPagina = document.getElementById("itens-por-pagina");
    const pagina_n = document.getElementById("pagina");

    if (rota !== '') {

        document.getElementById('visual-tabela').innerHTML = "";

        elementoContent.classList.remove('d-none');
        document.getElementById('visual-tabela').innerHTML = "<p>Carregando...</p>";
        jsonContainer.textContent = "Carregando...";

        let chamada = `http://10.1.6.2/relgcs/api/${rota}/lista.php`;

        if (
            parseInt(todos) == 1 || 
            ( 
                rota == 'atividades/status' || 
                rota == 'contratos/status' || 
                rota == 'fornecedores/ramos' ||
                rota == 'aeic/divisoes' ||
                rota == 'aeic/responsaveis'
            )
        ) {
            chamada += `${URLInteligente(chamada)}todos=1`;
        } else {
            if (por_pagina) {
                chamada += `${URLInteligente(chamada)}por_pagina=${parseInt(por_pagina == "" ? 20 : por_pagina)}`;
            }
    
            if (pagina !== undefined && pagina !== '' && String(pagina) !== 'NaN') {
                if (parseInt(pagina) != "") {
                    chamada += `${URLInteligente(chamada)}pagina=${parseInt(pagina)}`;
                } else {
                    chamada += `${URLInteligente(chamada)}pagina=${parseInt(pagina)}`;
                }
            }
        }

        if (rota == 'atividades') {
            if (local_atividade) {
                chamada += `${URLInteligente(chamada)}local=${parseInt(local_atividade)}`;
            }
            if (status_atividade) {
                chamada += `${URLInteligente(chamada)}status=${parseInt(status_atividade)}`;
            }
        }

        if (rota == 'contratacoes') {
            if (id_contratacao) {
                chamada += `${URLInteligente(chamada)}contratacao=${id_contratacao}`;
            }
            if (responsavel_contratacao) {
                chamada += `${URLInteligente(chamada)}responsavel=${responsavel_contratacao}`;
            }
        }

        if (rota == 'contratos') {
            if (divisao_contrato) {
                chamada += `${URLInteligente(chamada)}divisao=${divisao_contrato}`;
            }
            if (status_contrato) {
                chamada += `${URLInteligente(chamada)}status=${status_contrato}`;
            }
            if (responsavel_contrato) {
                chamada += `${URLInteligente(chamada)}responsavel=${responsavel_contrato}`;
            }
            if (tipo_contrato) {
                chamada += `${URLInteligente(chamada)}tipo=${tipo_contrato}`;
            }
            if (doc_contratual_contrato) {
                chamada += `${URLInteligente(chamada)}doc_contratual=${doc_contratual_contrato}`;
            }
        }

        if (rota == 'fornecedores') {
            if (situacao_fornecedor >= 0) {
                chamada += `${URLInteligente(chamada)}situacao=${situacao_fornecedor}`;
            }
            if (porte_fornecedor) {
                chamada += `${URLInteligente(chamada)}porte=${porte_fornecedor}`;
            }
            if (ramo_fornecedor) {
                chamada += `${URLInteligente(chamada)}ramo=${ramo_fornecedor}`;
            }
        }

        if (rota == 'aeic') {
            if (divisao_aeic) {
                chamada += `${URLInteligente(chamada)}divisao=${divisao_aeic}`;
            }
            if (status_aeic) {
                chamada += `${URLInteligente(chamada)}status=${status_aeic}`;
            }
            if (responsavel_aeic) {
                chamada += `${URLInteligente(chamada)}responsavel=${responsavel_aeic}`;
            }
            if (tipo_aeic) {
                chamada += `${URLInteligente(chamada)}tipo=${tipo_aeic}`;
            }
            if (doc_contratual_aeic) {
                chamada += `${URLInteligente(chamada)}doc_contratual=${doc_contratual_aeic}`;
            }
        }

        fetch(chamada)  // Substitua pelo seu endpoint
        .then(response => response.json())
        .then(data => {

            CriadorDeTabelas({
                area: 'visual-tabela',
                tabela: rota
            });

            if (todos != 0 || todos === undefined) {

                PaginasDisponiveis(data?.pages);
                itemsPorPagina.value = data?.per_page;
                pagina_n.value = data?.page;

                if (data?.qty_total > data?.per_page) {
                    itemsPorPagina.classList.remove('d-none');
                    pagina_n.classList.remove('d-none');
                } else {
                    itemsPorPagina.classList.add('d-none');
                    pagina_n.classList.add('d-none');

                    itemsPorPagina.value = '';
                    pagina_n.value = '';
                }

                TabelasHTML({
                    id: 'tabela-dados',
                    tabela: rota,
                    dados: data?.results
                })
                
            }    
            
            document.getElementById("executar").value = 1;
            const formattedJson = JSON.stringify(data, null, 2);
            jsonContainer.textContent = formattedJson;
            hljs.highlightBlock(jsonContainer);
            filename.value = 'dados_de_'+rota.replace('/','_').toLowerCase()+'_'+data?.last_update.replaceAll('-','_').replaceAll(':','_').replaceAll(' ','_');
            linkRota.value = chamada;
            elementoRota.classList.remove('d-none');
            elementoShare.classList.remove('d-none');

            setTimeout(()=>{
                elementoShareArea.classList.remove('d-none');
            },1000)
        
        })
        .catch(error => {
            console.error("Error fetching data:", error);
            jsonContainer.textContent = "Error fetching data.";
        });
    } else {
        elementoContent.classList.add('d-none');
    }
}

function PaginasDisponiveis(paginas = 1) {
    const paginador = document.getElementById("pagina");
    let valor = paginador.value
    let opcoes = ``;
    for (let p = 1; p <= paginas; p++) {
        opcoes += valor == p ? `<option value="${p}" selected>Página ${p}</option>` : `<option value="${p}">Página ${p}</option>`;
    }
    paginador.innerHTML = opcoes;  
}

function handleSubmit() {
    const selectedOption = document.getElementById("rota")?.value;
    const tipoDeExibicao = document.getElementById("conteudo")?.value;
    const itemsPorPagina = document.getElementById("itens-por-pagina")?.value;
    const pagina = document.getElementById("pagina")?.value;

    let requisicao = {
        rota: selectedOption
    }

    if (tipoDeExibicao != null && tipoDeExibicao != undefined && tipoDeExibicao != 0) {
        requisicao.todos = parseInt(tipoDeExibicao);
    }

    if (itemsPorPagina != null && itemsPorPagina != undefined && itemsPorPagina != "") {
        requisicao.por_pagina = parseInt(itemsPorPagina);
    }

    if (pagina != null && pagina != undefined && pagina != 1) {
        requisicao.pagina = parseInt(pagina);
    }

    if (selectedOption == 'atividades') {
    
        const localAtividade = document.getElementById('local-atividade')?.value;
        const statusAtividade = document.getElementById('status-atividade')?.value;

        if (localAtividade != null && localAtividade != undefined && localAtividade != "") {
            requisicao.local_atividade = parseInt(localAtividade);
        }

        if (statusAtividade != null && statusAtividade != undefined && statusAtividade != "") {
            requisicao.status_atividade = parseInt(statusAtividade);
        }

    }

    if (selectedOption == 'contratacoes') {

        const idContratacao = document.getElementById('id-contratacao')?.value;
        const responsavelContratacao = document.getElementById('responsavel-contratacao')?.value;

        if (idContratacao != null && idContratacao != undefined && idContratacao != "") {
            requisicao.id_contratacao = idContratacao;
        }

        if (responsavelContratacao != null && responsavelContratacao != undefined && responsavelContratacao != "") {
            requisicao.responsavel_contratacao = responsavelContratacao;
        }

    }

    if (selectedOption == 'contratos') {

        const divisao_contrato = document.getElementById('divisao-contrato')?.value;
        const status_contrato = document.getElementById('status-contrato')?.value;
        const responsavel_contrato = document.getElementById('responsavel-contrato')?.value;
        const tipo_contrato = document.getElementById('tipo-contrato')?.value;
        const doc_contratual_contrato = document.getElementById('doc-contratual-contrato')?.value;
        
        if (divisao_contrato != null && divisao_contrato != undefined && divisao_contrato != "") {
            requisicao.divisao_contrato = parseInt(divisao_contrato);
            delete requisicao.por_pagina;
        }

        if (status_contrato != null && status_contrato != undefined && status_contrato != "") {
            requisicao.status_contrato = parseInt(status_contrato);
            delete requisicao.por_pagina;
        }

        if (responsavel_contrato != null && responsavel_contrato != undefined && responsavel_contrato != "") {
            requisicao.responsavel_contrato = parseInt(responsavel_contrato);
            delete requisicao.por_pagina;
        }

        if (tipo_contrato != null && tipo_contrato != undefined && tipo_contrato != "") {
            requisicao.tipo_contrato = parseInt(tipo_contrato);
            delete requisicao.por_pagina;
        }

        if (doc_contratual_contrato != null && doc_contratual_contrato != undefined && doc_contratual_contrato != "") {
            requisicao.doc_contratual_contrato = doc_contratual_contrato;

            delete requisicao.por_pagina;
            delete requisicao.pagina;
            delete requisicao.status;
        }

    }

    if (selectedOption == 'fornecedores') {

        const situacao_fornecedor = document.getElementById('situacao-fornecedor')?.value;
        const porte_fornecedor = document.getElementById('porte-fornecedor')?.value;
        const ramo_fornecedor = ramosDeFornecimento.getValue();

        let ramo_fornecedor_string = ramo_fornecedor.map(item => item.id).join(',');

        if (situacao_fornecedor != null && situacao_fornecedor != undefined && situacao_fornecedor != "") {
            requisicao.situacao_fornecedor = parseInt(situacao_fornecedor);
        }

        if (porte_fornecedor != null && porte_fornecedor != undefined && porte_fornecedor != "") {
            requisicao.porte_fornecedor = parseInt(porte_fornecedor);
        }

        if (ramo_fornecedor != null && ramo_fornecedor != undefined && ramo_fornecedor.length > 0) {
            requisicao.ramo_fornecedor = ramo_fornecedor_string;
        }

    }

    if (selectedOption == 'aeic') {

        const divisao_aeic = document.getElementById('divisao-aeic')?.value;
        const status_aeic = document.getElementById('status-aeic')?.value;
        const responsavel_aeic = document.getElementById('responsavel-aeic')?.value;
        const tipo_aeic = document.getElementById('tipo-aeic')?.value;
        const doc_contratual_aeic = document.getElementById('doc-contratual-aeic')?.value;

        if (divisao_aeic != null && divisao_aeic != undefined && divisao_aeic != "") {
            requisicao.divisao_aeic = parseInt(divisao_aeic);
        }

        if (status_aeic != null && status_aeic != undefined && status_aeic != "") {
            requisicao.status_aeic = parseInt(status_aeic);
        }

        if (responsavel_aeic != null && responsavel_aeic != undefined && responsavel_aeic != "") {
            requisicao.responsavel_aeic = parseInt(responsavel_aeic);
        }

        if (tipo_aeic != null && tipo_aeic != undefined && tipo_aeic != "") {
            requisicao.tipo_aeic = parseInt(tipo_aeic);
        }

        if (doc_contratual_aeic != null && doc_contratual_aeic != undefined && doc_contratual_aeic != "") {
            requisicao.doc_contratual_aeic = doc_contratual_aeic;

            delete requisicao.por_pagina;
            delete requisicao.pagina;
            delete requisicao.status;
        }


    }    

    FazerRequisicao(requisicao)    
}

function ExecutarURL() {
    const paramotros_url = new URLSearchParams(window.location.search);
    let verifica_se_e_autoexecutavel = paramotros_url.get('executar')
    
    if (verifica_se_e_autoexecutavel == '1') {
        handleSubmit();
    }
}

function handleSelectChange(muda_rota = 0) {
    const selectedOption = document.getElementById("rota").value;
    const tipoDeExibicao = document.getElementById("conteudo");

    const elementoRota = document.getElementById("ferramentas");
    const elementoContent = document.getElementById("content");

    const filtros = document.getElementById("filtros");
    const bt_filtros = document.getElementById("bt-send-filtros");

    const elementoShareArea = document.getElementById("area-de-compartilhamento");

    elementoShareArea.classList.add('d-none');

    const filtro_atividades = document.getElementById("filtros-atividades");
    const filtro_contratacoes = document.getElementById("filtros-contratacoes");
    const filtro_contratos = document.getElementById("filtros-contratos");
    const filtro_contratos_2 = document.getElementById("filtros-contratos-2");
    const filtro_fornecedores = document.getElementById("filtros-fornecedores");
    const filtro_fornecedores_2 = document.getElementById("filtros-fornecedores-2");
    const filtro_aeic = document.getElementById("filtros-aeic");
    const filtro_aeic_2 = document.getElementById("filtros-aeic-2");

    const elementoShare = document.getElementById("share");

    const paramotros_url = new URLSearchParams(window.location.search);
    let url_atual = `http://ditnet/relgcs/api/assistente/?mode=${paramotros_url.get('mode')}`  
    

    if (selectedOption != "") {
        filtros.classList.remove('d-none');
        bt_filtros.classList.remove('d-none');
        tipoDeExibicao.classList.remove('d-none');

        if (muda_rota == 1) {
            elementoContent.classList.add("d-none");
            elementoRota.classList.add("d-none");
        }


        if ( 
            selectedOption == 'locais' || 
            selectedOption == 'atividades/status' || 
            selectedOption == "contratos/status" ||
            selectedOption == "aeic/divisoes" ||
            selectedOption == "aeic/responsaveis"
        ) {
            filtros.classList.add('d-none');
            filtro_atividades.classList.add('d-none');
            filtro_contratacoes.classList.add('d-none');
            filtro_contratos.classList.add('d-none');
            filtro_contratos_2.classList.add('d-none');
            filtro_fornecedores.classList.add('d-none');
            filtro_fornecedores_2.classList.add('d-none');
            filtro_aeic.classList.add('d-none');
            filtro_aeic_2.classList.add('d-none');
        }

        if (selectedOption == 'atividades') {
            filtros.classList.remove('d-none');
            filtro_atividades.classList.remove('d-none');
            filtro_contratacoes.classList.add('d-none');
            filtro_contratos.classList.add('d-none');
            filtro_contratos_2.classList.add('d-none');
            filtro_fornecedores.classList.add('d-none');
            filtro_fornecedores_2.classList.add('d-none');
            filtro_aeic.classList.add('d-none');
            filtro_aeic_2.classList.add('d-none');
        }

        if (selectedOption == 'contratacoes') {
            filtros.classList.remove('d-none');
            filtro_atividades.classList.add('d-none');
            filtro_contratacoes.classList.remove('d-none');
            filtro_contratos.classList.add('d-none');
            filtro_contratos_2.classList.add('d-none');
            filtro_fornecedores.classList.add('d-none');
            filtro_fornecedores_2.classList.add('d-none');
            filtro_aeic.classList.add('d-none');
            filtro_aeic_2.classList.add('d-none');
        }

        if (selectedOption == 'contratos') {
            filtros.classList.remove('d-none');
            filtro_atividades.classList.add('d-none');
            filtro_contratacoes.classList.add('d-none');
            filtro_contratos.classList.remove('d-none');
            filtro_contratos_2.classList.remove('d-none');
            filtro_fornecedores.classList.add('d-none');
            filtro_fornecedores_2.classList.add('d-none');
            filtro_aeic.classList.add('d-none');
            filtro_aeic_2.classList.add('d-none');
        }

        if (selectedOption == 'fornecedores') {
            filtros.classList.remove('d-none');
            filtro_atividades.classList.add('d-none');
            filtro_contratacoes.classList.add('d-none');
            filtro_contratos.classList.add('d-none');
            filtro_contratos_2.classList.add('d-none');
            filtro_fornecedores.classList.remove('d-none');
            filtro_fornecedores_2.classList.remove('d-none');
            filtro_aeic.classList.add('d-none');
            filtro_aeic_2.classList.add('d-none');
        }

        if (selectedOption == 'aeic') {
            filtros.classList.remove('d-none');
            filtro_atividades.classList.add('d-none');
            filtro_contratacoes.classList.add('d-none');
            filtro_contratos.classList.add('d-none');
            filtro_contratos_2.classList.add('d-none');
            filtro_fornecedores.classList.add('d-none');
            filtro_fornecedores_2.classList.add('d-none');
            filtro_aeic.classList.remove('d-none');
            filtro_aeic_2.classList.remove('d-none');
        }

        url_atual = new URL(url_atual);
        url_atual.searchParams.set('option',selectedOption.replace('/','_'));

        switch (selectedOption.replace('/','_')) {
            case '':
                document.title = 'SiGi API Assist';
                break;

            case 'atividades_status':
                document.title = 'SiGi API Assist | Status de Atividades';
                break;

            case 'atividades':
                document.title = 'SiGi API Assist | Atividades';
                break;

            case 'locais':
                document.title = 'SiGi API Assist | Locais';
                break;

            case 'contratacoes':
                document.title = 'SiGi API Assist | Contratações';
                break;

            case 'contratos_status':
                document.title = 'SiGi API Assist | Status de Contratos';
                break;

            case 'fornecedores_ramos':
                document.title = 'SiGi API Assist | Ramos de Fornecedores';
                break;

            case 'fornecedores':
                document.title = 'SiGi API Assist | Fornecedores';
                break;

            case 'aeic_divisoes':
                document.title = 'SiGi API Assist | Divisões';
                break;

            case 'aeic_responsaveis':
                document.title = 'SiGi API Assist | Responsávels dos Contratos';
                break;

            case 'aeic':
                document.title = 'SiGi API Assist | A.E.I.C.s';
                break;
            
            default:
                document.title = 'SiGi API Assist';
                break;
        }

        // Capturar campos relevantes e incluí-los na URL
        const parametros = MontaURLComParametros(url_atual);

        if (parametros) {
            url_atual = parametros;
        }
        
    } else {
        filtros.classList.add('d-none');
        tipoDeExibicao.classList.add('d-none');
        filtro_atividades.classList.add('d-none');
        filtro_contratacoes.classList.add('d-none');
        filtro_contratos.classList.add('d-none');
        filtro_contratos_2.classList.add('d-none');
        filtro_fornecedores.classList.add('d-none');
        filtro_fornecedores_2.classList.add('d-none');
        filtro_aeic.classList.add('d-none');
        filtro_aeic_2.classList.add('d-none');
        bt_filtros.classList.add('d-none');
        elementoContent.classList.add('d-none');
        elementoShare.classList.add('d-none');

        if (selectedOption == '') {
            document.title = 'SiGi API Assist';
        }
        
    }

    window.history.pushState(null, null, url_atual)
    //FazerRequisicao(selectedOption);

    if (document.getElementById("executar").value === '1') {
        setTimeout(()=>{
            //console.log('Testando');
            handleSubmit();
        },1000)
    }

}

handleSelectChange();

function MontaURLComParametros(url_atual = '', rota = '') {
    let newUrl = new URL(url_atual);
    
    const campos = document.querySelectorAll('select, input, textarea');

    campos.forEach(campo => {
        const id = campo.id;
        const value = campo.value;
        if (id && ( value !== null && value !== '') && (
            id != 'rota' &&
            id != 'filename' &&
            id != 'executar'
        )) {
            newUrl.searchParams.set(id, value);
        }
    });

    return newUrl;
}

function downloadJson() {
    const json = document.getElementById("json-container").textContent;
    const filename = document.getElementById("filename").value;
    const blob = new Blob([json], { type: "application/json" });
    const url = URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = filename + ".json";  // Nome personalizado + extensão
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

function flattenObject(obj, parentKey = "") {
    const result = {};

    for (const key in obj) {
        const value = obj[key];

        if (Array.isArray(value)) {
            value.forEach((item, index) => {
                const itemKey = parentKey + `[${index}]`;
                Object.assign(result, flattenObject(item, itemKey));
            });
        } else if (typeof value === "object" && value !== null) {
            Object.assign(result, flattenObject(value, parentKey + (parentKey ? "." : "") + key));
        } else {
            result[parentKey + (parentKey ? "." : "") + key] = value;
        }
    }

    return result;
}

function convertJSONToExcel(jsonData) {
    const flattenedData = jsonData.map(item => flattenObject(item));
    const keys = Array.from(new Set(flattenedData.flatMap(item => Object.keys(item))));
    const wsData = [keys, ...flattenedData.map(item => keys.map(key => item[key] || ""))];

    const ws = XLSX.utils.aoa_to_sheet(wsData);
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    return wb;
}

function downloadExcel(wb, filename) {
    const wbout = XLSX.write(wb, { bookType: "xlsx", type: "array" });
    const blob = new Blob([wbout], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
    const url = URL.createObjectURL(blob);

    const a = document.createElement('a');
    a.href = url;
    a.download = filename + '.xlsx';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    URL.revokeObjectURL(url);
}

function baixarExcel() {
    const jsonData = JSON.parse(document.getElementById("json-container").textContent).results;
    console.log('XLSX: ',jsonData);
    const filename = document.getElementById("filename").value;
    const workbook = convertJSONToExcel(jsonData);
    downloadExcel(workbook, filename);
}

function mergeCells(worksheet, startRow, startCol, endRow, endCol) {
    const merges = worksheet['!merges'] || [];
    merges.push({ s: { r: startRow, c: startCol }, e: { r: endRow, c: endCol } });
    worksheet['!merges'] = merges;
}

function exportToExcelFromTable() {
    const tabela = document.getElementById('tabela-dados'); // Substitua pelo ID da sua tabela
    const nomeArquivo = document.getElementById("filename").value+'.xlsx'; // Nome do arquivo de saída
    
    const wb = XLSX.utils.table_to_book(tabela);

    const ws = wb.Sheets[wb.SheetNames[0]];

    for (let row = 0; row < tabela.rows.length; row++) {
        for (let col = 0; col < tabela.rows[row].cells.length; col++) {
        const cell = tabela.rows[row].cells[col];

        if (cell.querySelector('table')) {
            const endRow = row + cell.rowSpan - 1;
            const endCol = col + cell.colSpan - 1;

            mergeCells(ws, row, col, endRow, endCol);
        }
        }
    }

    XLSX.writeFile(wb, nomeArquivo);
}

document.getElementById("btn-copiar").addEventListener("click", function() {
    const rotaDeChamada = document.querySelector(".rota-de-chamada");
    const notificacao = document.getElementById("notificacao-copy");
    notificacao.classList.add('show');
    setTimeout(()=>{
        notificacao.classList.remove('show');
    },3000);
    rotaDeChamada.select();
    document.execCommand("copy");
});

// Evento de carregamento da página
window.addEventListener("load", function() {
    const initialOption = document.getElementById("rota").value;
    FazerRequisicao(initialOption);
});

function URLInteligente(url = '') {
    if (url.includes('?')) {
        return '&';
      } else {
        return '?';
    }
}

function BuscaDados(rota = '') {
    return new Promise(function(resolve, reject) {
        if (rota != '') {
            fetch(`http://10.1.6.2/relgcs/api/${rota}/lista.php?todos=1`)  // Substitua pelo seu endpoint
            .then(response => response.json())
            .then(({ results } = data) => {
                //console.log('Resultados: ',results)
                //return ;
                resolve(results);
            })
            .catch(error => {
                console.error("Error fetching data:", error);
                //jsonContainer.textContent = "Error fetching data.";
                //return [];
                reject(error);
            });
        }
    })
}

function CarregaOpcoesDeLocaisAtividades() {

    const locais = document.getElementById("local-atividade");
    let opcoes = `<option value="" selected>Filtrar por local</option>`;    
    BuscaDados('locais')
    .then(dados => {
        dados.map(item => {
            opcoes += `<option value="${item.id}">${item.local}</option>`;
        })
        locais.innerHTML = opcoes;    
    })
    .catch(e=>console.error(e))    

}

function CarregaOpcoesDeStatusAtividades() {

    const locais = document.getElementById("status-atividade");
    let opcoes = `<option value="" selected>Filtrar por status</option>`;    
    BuscaDados('atividades/status')
    .then(dados => {
        dados.map(item => {
            opcoes += `<option value="${item.id}">${item.status}</option>`;
        })
        locais.innerHTML = opcoes;    
    })
    .catch(e=>console.error(e))    

}

function CarregaOpcoesDeStatusContratos() {

    const locais = document.getElementById("status-contrato");
    let opcoes = `<option value="" selected>Filtrar por status</option>`;    
    BuscaDados('contratos/status')
    .then(dados => {
        dados.map(item => {
            opcoes += `<option value="${item.id}">${item.status}</option>`;
        })
        locais.innerHTML = opcoes;    
    })
    .catch(e=>console.error(e))    

}

CarregaOpcoesDeLocaisAtividades();
CarregaOpcoesDeStatusAtividades();
CarregaOpcoesDeStatusContratos();

const elementChoices = document.querySelector('.js-choice');

const ramosDeFornecimento = new Choices(elementChoices, {
    itemSelectText: 'Adicionar ramo',
    noResultsText: 'Ramo não encontrado!',
    removeItems: true,
    removeItemButton: true,
    classNames: {
        containerOuter: 'choices form-select w-100 py-0 rounded-5 mb-2 mb-md-0',
        containerInner: 'choices__inner border-0 bg-white d-flex flex-column p-0',
        input: 'form-control',
        inputCloned: 'choices__input--cloned mb-0 border-0 border-none mt-1 bg-none mx-auto w-100 text-center',
        list: 'choices__list',
        listItems: 'choices__list--multiple',
        listSingle: 'choices__list--single',
        listDropdown: 'choices__list--dropdown',
        item: 'choices__item',
        itemSelectable: 'choices__item--selectable',
        itemDisabled: 'choices__item--disabled',
        itemChoice: 'choices__item--choice',
        placeholder: 'choices__placeholder',
        group: 'choices__group',
        groupHeading: 'choices__heading',
        button: 'choices__button',
        activeState: 'is-active',
        focusState: 'is-focused',
        openState: 'is-open',
        disabledState: 'is-disabled',
        highlightedState: 'is-highlighted',
        selectedState: 'is-selected',
        flippedState: 'is-flipped',
        loadingState: 'is-loading',
        noResults: 'has-no-results',
        noChoices: 'has-no-choices'
      },
});

ramosDeFornecimento.setChoices(async () => {
    try {
        const response = await fetch('http://10.1.6.2/relgcs/api/fornecedores/ramos/lista.php?todos=1');
        const data = await response.json();
        // Extrair as opções da resposta da API
        const options = data.results.map(item => ({ value: item.id, label: item.ramo }));
        return options;
    } catch (err) {
      console.error(err);
    }
});

function CarregaOpcoesDeDivisoesContratos() {

    const locais = document.getElementById("divisao-contrato");
    let opcoes = `<option value="">Filtrar por divisão</option>`;    
    BuscaDados('aeic/divisoes')
    .then(dados => {
        dados.map(item => {
            opcoes += `<option value="${item.id}">${item.divisao}</option>`;
        })
        locais.innerHTML = opcoes;    
    })
    .catch(e=>console.error(e))    

}

function CarregaOpcoesDeStatusContratos() {

    const locais = document.getElementById("status-contrato");
    let opcoes = `<option value="" selected>Filtrar por status</option>`;    
    BuscaDados('contratos/status')
    .then(dados => {
        dados.map(item => {
            opcoes += `<option value="${item.id}">${item.status}</option>`;
        })
        locais.innerHTML = opcoes;    
    })
    .catch(e=>console.error(e))    

}

function CarregaOpcoesDeResponsaveisContratos() {

    const locais = document.getElementById("responsavel-contrato");
    let opcoes = `<option value="">Filtrar por responsavel</option>`;    
    BuscaDados('aeic/responsaveis')
    .then(dados => {
        dados.map(item => {
            opcoes += item.responsavel != undefined ? `<option value="${item.id}">${item.responsavel}</option>` : ``;
        })
        locais.innerHTML = opcoes;    
    })
    .catch(e=>console.error(e))    

}

CarregaOpcoesDeDivisoesContratos();
CarregaOpcoesDeStatusContratos();
CarregaOpcoesDeResponsaveisContratos();

function CarregaOpcoesDeDivisoesAEIC() {

    const locais = document.getElementById("divisao-aeic");
    let opcoes = `<option value="">Filtrar por divisão</option>`;    
    BuscaDados('aeic/divisoes')
    .then(dados => {
        dados.map(item => {
            opcoes += `<option value="${item.id}">${item.divisao}</option>`;
        })
        locais.innerHTML = opcoes;    
    })
    .catch(e=>console.error(e))    

}

function CarregaOpcoesDeStatusAEIC() {

    const locais = document.getElementById("status-aeic");
    let opcoes = `<option value="" selected>Filtrar por status</option>`;    
    BuscaDados('contratos/status')
    .then(dados => {
        dados.map(item => {
            opcoes += `<option value="${item.id}">${item.status}</option>`;
        })
        locais.innerHTML = opcoes;    
    })
    .catch(e=>console.error(e))    

}

function CarregaOpcoesDeResponsaveisAEIC() {

    const locais = document.getElementById("responsavel-aeic");
    let opcoes = `<option value="">Filtrar por responsavel</option>`;    
    BuscaDados('aeic/responsaveis')
    .then(dados => {
        dados.map(item => {
            opcoes += item.responsavel != undefined ? `<option value="${item.id}">${item.responsavel}</option>` : ``;
        })
        locais.innerHTML = opcoes;    
    })
    .catch(e=>console.error(e))    

}

CarregaOpcoesDeDivisoesAEIC();
CarregaOpcoesDeStatusAEIC();
CarregaOpcoesDeResponsaveisAEIC();

function Visualizacao(parametros = {
    modo: 'dados'
}) {
    const {
        modo = 'dados'
    } = parametros

    const content_dados = document.getElementById('visual-dados')
    const content_tabela = document.getElementById('visual-tabela')

    if (modo == 'dados') {
        content_dados.classList.remove('d-none');
        content_tabela.classList.add('d-none');
    }

    if (modo == 'tabela') {
        content_dados.classList.add('d-none');
        content_tabela.classList.remove('d-none');
    }
}

function CriadorDeTabelas(parametros = {
    area,
    tabela
}) {
    const {
        area,
        tabela
    } = parametros

    if (document.getElementById(area) !== null) {
        document.getElementById(area).innerHTML = `<table id="tabela-dados" class="tabela-${tabela}">
            <thead></thead>
            <tbody></tbody>
        </table>`;
    }
}

CriadorDeTabelas({
    area: 'visual-tabela'
});

function DefineTitulosDaTabela(parametros = {
    area,
    dados,
    modelo
}) {
    const {
        area,
        dados = [],
        modelo = 'atributo'
    } = parametros

    const titulos = area.getElementsByTagName('thead')[0];

    if( dados.length > 0 ) {

        let linha_tilulo = `<tr>`;

            if (modelo == 'atributo') {

                const nomesDosAtributos = [];

                // Loop através do primeiro objeto para capturar os nomes dos atributos
                for (const nomeDoAtributo in dados[0]) {
                    nomesDosAtributos.push(nomeDoAtributo);
                }

                nomesDosAtributos.map(titulo => {
                    linha_tilulo += `<th class="bg-primary text-white">${titulo.replaceAll('_',' ')}</th>`;
                })

                

            }
            
            if (modelo == 'matriz') {
                for (let i = 0; i < dados.length; i++) {
                    linha_tilulo += `<th class="bg-primary text-white ${dados[i]?.class || ''}" colspan="${dados[i]?.colspan || ''}">${dados[i]?.label.replaceAll('_',' ')}</th>`;
                }
            }

        linha_tilulo += `</th>`;

        titulos.innerHTML = linha_tilulo;

    } 
}

function criaTabela(dados,rota) {
    let conteudo_interno = '';

    conteudo_interno += `<table class="layout-tabela ${rota}">`;
    // Cabeçalhos baseados nos rótulos das propriedades dos objetos JSON
    conteudo_interno += `<thead><tr>`;
    for (const prop in dados[0]) {
        conteudo_interno += `<th class="subtitles bg-info text-white">${prop.replaceAll('_', ' ')}</th>`;
    }
    conteudo_interno += `</tr></thead>`;

    // Conteúdo das células internas
    conteudo_interno += `<tbody>`;
    for (const item of dados) {
        conteudo_interno += `<tr>`;
        for (const prop in item) {
            const valor = item[prop];
            if (Array.isArray(valor) && typeof valor[0] === 'object') {
                conteudo_interno += `<td class="remove-padding">${criaTabela(valor)}</td>`;
            } else if (typeof valor === 'object') {
                conteudo_interno += `<td>`;
                for (const chave in valor) {
                    conteudo_interno += `<b class="text-uppercase">${chave.replaceAll('_', ' ')}:</b> ${valor[chave] !== null ? valor[chave] : ''}<br>`;
                }
                conteudo_interno += `</td>`;
            } else {
                conteudo_interno += `<td>${valor !== null ? valor : ''}</td>`;
            }
        }
        conteudo_interno += `</tr>`;
    }
    conteudo_interno += `</tbody>`;

    conteudo_interno += `</table>`;

    return conteudo_interno;
}

function criaMesclagem(dados,sub_grupom,n_sub) {
    let conteudo_interno = '';

    //console.log('N SUB: ',n_sub)
    
    for (const prop in dados[0]) {
        conteudo_interno += prop != 'n_sub' ? `<td class="subtitles bg-info text-white" colspan="${prop == 'aeics' ? 3 : `1`}">${prop.replaceAll('_', ' ').toUpperCase()}</td>` : ``;
    }

    conteudo_interno += dados[0] ? `</tr>` : ``;

    for (let i = 0; i < dados.length; i++) {
        conteudo_interno += `<tr>`;

        //const rowspan = typeof dados[i]?.n_sub === 'number' ? dados[i]?.n_sub+2 : 2;
        const rowspan = dados.length === i ? parseInt(n_sub) + 1 : 1;

        //console.log('SUB SUB: ',dados[i]?.n_sub,n_sub,dados.length)

        if (dados[i]?.n_sub == 0 && sub_grupo == 'contratacoes') {

            conteudo_interno += `<td rowspan="${rowspan}" colspan="17"></td></tr><tr>`

        } else {

            for (const valor in dados[i]) {
                const item = dados[i][valor];
                
                if (Array.isArray(item) && typeof item[0] === 'object') {
                    //conteudo_interno += valor != 'n_sub' ? `<td class="remove-padding" rowspan="${rowspan}">${criaTabela(item,parametros.rota)}</td>` : ``;
                    //conteudo_interno += valor != 'n_sub' ? `<tr>${criaMesclagem(item)}` : ``;
                    conteudo_interno += valor != 'n_sub' ? `${criaMesclagem(item)}` : ``;
                } else if (typeof item === 'object') {
                    if (valor != 'n_sub') {
                        conteudo_interno += `<td rowspan="${rowspan}" class="${valor === 'escopo' || valor === 'observacao' ? 'obs-col' : ''}">`;
                        for (const chave in item) {
                            conteudo_interno += `${chave}: ${item[chave]}<br>`;
                        }
                        conteudo_interno += `</td>`;
                    }                    
                } else {
                    conteudo_interno += valor != 'n_sub' ? `<td rowspan="${rowspan}" class="${valor === 'escopo' || valor === 'observacao' ? 'obs-col' : ''}">${item !== 'null' ? item : ''}</td>` : ``;
                }
            }
        }

        conteudo_interno += `</tr>`;
    }

    return conteudo_interno;
}

function ConteudoTabela(parametros = {
    area: null,
    rota: '',
    dados: []
}) {
    const conteudo = parametros.area.getElementsByTagName('tbody')[0];

    if (parametros.dados.length > 0) {
        let conteudo_interno = '';

        if (parametros.dados[0]?.camadas != undefined) {
            if (parametros.rota == 'aeic') {

                for (let i = 0; i < parametros.dados.length; i++) {
                
                    conteudo_interno += `<tr class="divisor"><td class="bg-primary text-white" colspan="1">ID</td><td colspan="4">${parametros.dados[i].id_contrato}</td></tr>`;
                    conteudo_interno += `<tr><td class="bg-primary text-white" colspan="1">DOC CONTRATUAL</td><td colspan="4">${parametros.dados[i].doc_contratual}</td></tr>`;
                    conteudo_interno += `<tr><td class="bg-primary text-white" colspan="1">DIVISÃO</td><td colspan="4">${parametros.dados[i].divisao}</td></tr>`;
                    conteudo_interno += `<tr><td class="bg-primary text-white" colspan="1">RESPONSÁVEL</td><td colspan="4">${parametros.dados[i].resp_contrato}</td></tr>`;
                    conteudo_interno += `<tr><td class="bg-primary text-white" colspan="1">STATUS</td><td colspan="4">${parametros.dados[i].status}</td></tr>`;
                    
                    conteudo_interno += `<tr>
                        <td class="bg-primary text-white text-center" colspan="5">ATIVIDADE</td>
                    </tr>`;

                    let atividades = parametros.dados[i].atividades

                    for (let ii = 0; ii < atividades.length; ii++) {

                        conteudo_interno += `<tr>`;
            
                        const rowspan = typeof atividades[ii]?.n_sub === 'number' ? parseInt(atividades[ii]?.n_sub) + 2 : 2;

                        conteudo_interno += `<tr>
                            <td class="bg-primary text-white" colspan="1">ID SiGi</td>
                            ${atividades[ii]?.aeics != undefined && atividades[ii]?.aeics.length > 0 ? `<td class="bg-primary text-white obs-col" colspan="1">Escopo</td>` : `<td class="bg-primary text-white obs-col" colspan="6">Escopo</td>`}
                            ${atividades[ii]?.aeics != undefined && atividades[ii]?.aeics.length > 0 ? `<td class="bg-primary text-white text-center" colspan="5" rowspan="2">AEICS</td>` : ``}
                        </tr>`;                        
            
                        for (const valor in atividades[ii]) {
                            const item = atividades[ii][valor];

                            if (atividades[ii].n_sub == 0 && valor == 'aeics') {

                                conteudo_interno += `<td rowspan="${rowspan}" colspan="3"></td></tr><tr>`

                            } else {

                                if (Array.isArray(item) && typeof item[0] === 'object') {
                                    //conteudo_interno += valor != 'n_sub' ? `<td class="remove-padding" rowspan="${rowspan}">${criaTabela(item,parametros.rota)}</td>` : ``;
                                    conteudo_interno += valor != 'n_sub' ? `<tr>${criaMesclagem(item,parametros.rota,parseInt(atividades[ii]?.n_sub))}` : ``;
                                } else if (typeof item === 'object') {
                                    if (valor != 'n_sub') {
                                        conteudo_interno += `<td rowspan="${rowspan}" class="${valor === 'escopo' || valor === 'observacao' ? 'obs-col' : ''}">`;
                                        for (const chave in item) {
                                            conteudo_interno += `${chave}: ${item[chave]}<br>`;
                                        }
                                        conteudo_interno += `</td>`;
                                    }                    
                                } else {
                                    conteudo_interno += valor != 'n_sub' ? `<td rowspan="${rowspan}" class="${valor === 'escopo' || valor === 'observacao' ? 'obs-col' : ''}">${item !== 'null' ? item : ''}</td>` : ``;
                                }

                            }
                            
                        }
            
                        conteudo_interno += `</tr>`;
                    }

                }
            }
        } else {
            for (let i = 0; i < parametros.dados.length; i++) {

                conteudo_interno += `<tr>`;
    
                const rowspan = typeof parametros.dados[i]?.n_sub === 'number' ? parametros.dados[i]?.n_sub+1 : parametros.dados[i].n_sub !== undefined ? parametros.dados[i]?.n_sub+1 : `1`;
    
                
                const sub = parametros.dados[i].n_sub
                
                for (const valor in parametros.dados[i]) {
                    const item = parametros.dados[i][valor];

                    if (parametros.rota == 'contratacoes' && sub == 0) {

                        conteudo_interno += ``;

                    } else {
                        if (Array.isArray(item) && typeof item[0] === 'object') {
                            //conteudo_interno += valor != 'n_sub' ? `<td class="remove-padding" rowspan="${rowspan}">${criaTabela(item,parametros.rota)}</td>` : ``
                            //conteudo_interno += valor != 'n_sub' ? `<tr>${criaMesclagem(item,parametros.rota,parametros.dados[i]?.n_sub)}` : ``;
                            conteudo_interno += valor != 'n_sub' ? `${criaMesclagem(item,parametros.rota,parametros.dados[i]?.n_sub)}` : ``;
                        } else if (typeof item === 'object') {
                            if (valor != 'n_sub') {
                                conteudo_interno += `<td rowspan="${rowspan}" class="${valor === 'escopo' || valor === 'observacao' ? 'obs-col' : ''}">`;
                                for (const chave in item) {
                                    conteudo_interno += `${chave}: ${item[chave]}<br>`;
                                }
                                conteudo_interno += `</td>`;
                            }                    
                        } else {
                            conteudo_interno += valor != 'n_sub' ? `<td rowspan="${rowspan}" class="${valor === 'escopo' || valor === 'observacao' ? 'obs-col' : ''}">${item !== 'null' ? item : ''}</td>` : ``;
                        }
                    }
                }
    
                conteudo_interno += `</tr>`;
            }
        }
        conteudo.innerHTML = conteudo_interno;
    }
}


function TabelasHTML(parametros = {
    id,
    tabela,
    dados: []
}) {
    const {
        id,
        tabela,
        dados = []
    } = parametros

    if (document.getElementById(id) !== null) {
        const elementoTabela = document.getElementById(id);
        
        //if (tabela == 'contratos') {

            DefineTitulosDaTabela({
                area: elementoTabela,
                dados: MatrizDeTitulos({tipo: tabela}),
                modelo: 'matriz'
            });

            ConteudoTabela({
                area: elementoTabela,
                rota: tabela,
                dados: dados
            });

        //}
    }   

}

function MatrizDeTitulos(parametros = {
    tipo
}) {
    const {
        tipo
    } = parametros

    if (tipo == 'atividades/status' || tipo == 'contratos/status') {
        return [
            {
                "label": "ID",
            },
            {
                "label": "Status",
            }
        ]
    }

    if (tipo == 'atividades') {
        return [
            {"label": "ID"},
            {"label": "codigo_atividade"},
            {"label": "local"},
            {"label": "fonte_orcamento"},
            {"label": "sub_pep"},
            {"label": "programa"},
            {"label": "gestor"},
            {"label": "coordenador"},
            {"label": "responsavel"},
            {"label": "id_contrato"},
            {"label": "escopo"},
            {"label": "data_energizacao_prevista"},
            {"label": "doc_autorizacao"},
            {"label": "natureza_investimento"},
            {"label": "informacoes_complementares"},
            {"label": "observacoes"},
            {"label": "valor_orcado"},
            {"label": "valor_realizado"},
            {"label": "data_inicio_previsto"},
            {"label": "data_termino_previsto"},
            {"label": "data_inicio_real"},
            {"label": "data_termino_real"},
            {"label": "status"},
            {"label": "data_cadastro"},
            {"label": "classificacao_contabil_tipo"},
            {"label": "classificacao_contabil"},
            {"label": "obra"},
            {"label": "rd"},
            {"label": "status_sap"},
            {"label": "obra_complemento"},
            {"label": "projeto_tipo"},
            {"label": "id_resp_backup"},
            {"label": "aditamento"},
            {"label": "ata_implantacao"},
            {"label": "ata_encerramento"},
            {"label": "dt_ata_imp"},
            {"label": "id_user"},
            {"label": "valor_contratado"},
            {"label": "data_termino_execucao"}
        ] 
    }

    if (tipo == 'locais') {
        return [
            {
                "label": "ID",
            },
            {
                "label": "Local",
            }
        ]
    }

    if (tipo == 'contratacoes') {
        return [
            {"label": "id"},
            {"label": "tipo"},
            {"label": "status"},
            {"label": "registro_sap"},
            {"label": "dt_ap_req"},
            {"label": "flag_ap_req"},
            {"label": "dt_aceite_req"},
            {"label": "flag_aceite_req"},
            {"label": "dt_env_cons_merc"},
            {"label": "flag_env_cons_merc"},
            {"label": "dt_recb_prop"},
            {"label": "flag_recb_prop"},
            {"label": "dt_recb_prop_an"},
            {"label": "flag_rec_prop_an"},
            {"label": "dt_con_ana_tec"},
            {"label": "flag_conc_ana_tec"},
            {"label": "dt_conc_neg"},
            {"label": "flag_conc_neg"},
            {"label": "dt_ass_cont"},
            {"label": "flag_ass_cont"},
            {"label": "data_cadastro"},
            {"label": "id_contrato"},
            {"label": "escopo"},
            {"label": "responsavel"},
            {"label": "prazo_execucao"},
            {"label": "observacao"},
            {"label": "requisicoes", "colspan" : 17,}
        ]
    }

    if (tipo == 'fornecedores/ramos') {
        return [
            {
                "label": "ID",
            },
            {
                "label": "Ramo",
            }
        ]
    }

    if (tipo == 'fornecedores') {
        return [
            {"label": "id"},
            {"label": "razao_social"},
            {"label": "obra_protecao"},
            {"label": "porte"},
            {"label": "acervo_epc"},
            {"label": "tensao"},
            {"label": "acervo_fiscalizacao"},
            {"label": "teve_contrato"},
            {"label": "endereco"},
            {"label": "cnpj"},
            {"label": "observacoes", "class" : 'obs-col'},
            {"label": "situacao"},
            {"label": "registro_sap"},
            {"label": "ramos"}
        ]        
    }

    if (tipo == 'aeic/divisoes') {
        return [
            {
                "label": "ID",
            },
            {
                "label": "Divisão",
            }
        ]      
    }

    if (tipo == 'aeic/responsaveis') {
        return [
            {
                "label": "ID",
            },
            {
                "label": "Responsável",
            }
        ]      
    }

    if (tipo == 'aeic') {
        /*return [
            {
                "label": "ID SiGi",
            },
            {
                "label": "Escopo",
            }
        ]*/    
    }

    if (tipo == 'contratos') {
        return [
            {
                "label": "ID",
            },
            {
                "label": "Tipo",
            },
            {
                "label": "Documento",
            },
            {
                "label": "Fornecedor",
            },
            {
                "label": "Data de cadastro",
            },
            {
                "label": "Data de inicio",
            },
            {
                "label": "Data de termino",
            },
            {
                "label": "Data de base",
            },
            {
                "label": "Status",
            },
            {
                "label": "Responsavel",
            },
            {
                "label": "Contratação",
            },
            {
                "label": "Art CT",
            },
            {
                "label": "Ordem de serviço",
            },
            {
                "label": "Assinatura",
            },
            {
                "label": "Recebimento provisório",
            },
            {
                "label": "Recebimento definitivo",
            },
            {
                "label": "Atestado execução",
            },
            {
                "label": "Data de vigência",
            },
            {
                "label": "Data da ordem de serviço",
            },
            {
                "label": "Data  do recebimento provisório"
            },
            {
                "label": "Data do recebimento definitivo",
            },
            {
                "label": "ID do usuário",
            },
            {
                "label": "Departamento responsável",
            },
            {
                "label": "Ata de implantação",
            },
            {
                "label": "Valor contratado",
            },
            {
                "label": "Aditamento",
            },
            {
                "label": "Escopo da contratação",
            },
            {
                "label": "Observação",
            },
            {
                "label": "Atividades",
                "colspan" : 11,
                "class" : 'text-center'
            }
        ]
        
    }
}

function validarEmail(email) {
    var padrao = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return padrao.test(email);
}

function ShareByEmail() {

    const destinatario = document.getElementById("email-share").value;

    if (validarEmail(destinatario)) {

        const url_sigi = window.location.href;

        var assunto = "Confira estes dados do SiGi";
        var corpo = `Olá, tudo bem? Verifique estes dados do SiGi que estou lhe encaminhando por meio do abaixo:\n\n ${url_sigi.replace('executar=0','executar=1')}`;
        var link = "mailto:" + destinatario + "?subject=" + encodeURIComponent(assunto) + "&body=" + encodeURIComponent(corpo);
        //window.location.href = link;

        window.open(link, "_blank");

        document.getElementById("email-share").value = '';

        /*myModalEl.addEventListener('hidden.bs.modal', event => {
            // do something...
        })
        myModalEl.hide();*/

        const myModalEl = document.getElementById('share-busca');
        const modal = bootstrap.Modal.getInstance(myModalEl)
        modal.hide();

        const modalParts = document.querySelectorAll('.modal-backdrop');
        modalParts.forEach(function(element) {
            element.classList.remove('modal-backdrop');
            element.classList.remove('fade');
            element.classList.remove('show');
            element.remove();
        });

        setTimeout(()=>{
            document.body.removeAttribute('style');
        },1000)

    } else {

        const notificacao = document.getElementById("notificacao-erro");
        notificacao.classList.add('show');
        setTimeout(()=>{
            notificacao.classList.remove('show');
        },3000);

    }

}

document.getElementById("share").addEventListener("click", function() {
    const test_url = window.location.href;

    // Define as dimensões do popup
    const popupWidth = 700;
    const popupHeight = 600;

    // Calcula as coordenadas X e Y para centralizar o popup
    const left = (window.innerWidth - popupWidth) / 2;
    const top = (window.innerHeight - popupHeight) / 2;

    window.open(
        `https://teams.microsoft.com/share?assignTitle=Minha%20busca%20na%20ferramenta%20SiGi&href=${encodeURIComponent(test_url+'&executar=1')}&msgText=Ol%C3%A1%2C%20tudo%20bem%3F%20Verifique%20estes%20dados%20do%20SiGi%20que%20estou%20lhe%20encaminhando%20por%20meio%20do%20abaixo%3A&referrer=ditnet`,
        "ms-teams-share-popup",
        `width=${popupWidth},height=${popupHeight},left=${left},top=${top}`
    );
})
