document.addEventListener('keydown', function(event) {
    // Verifique se a tecla 'Alt' está pressionada juntamente com a tecla desejada
    if (event.altKey) {
        if (event.key === 'a' || event.key === 'A') {
            /*let selectElement = document.getElementById('rota');
            selectElement.click();
            selectElement.focus();*/

            const element = document.getElementById('rota');
            const event = new MouseEvent("mousedown");
            element.dispatchEvent(event);
            element.click();
            //element.size = element.options.length;
        }

        if (event.key === 'c' || event.key === 'C') {
            let selectElement = document.getElementById('btn-copiar');
            selectElement.click();
        }

        if (event.key === 'd' || event.key === 'D') {
            let selectElement = document.getElementById('download-button-d');
            selectElement.click();
        }

        if (event.key === 'e' || event.key === 'E') {
            let selectElement = document.getElementById('download-button-e');
            selectElement.click();
        }
    }
});
