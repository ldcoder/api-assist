<!DOCTYPE html>
<html lang="en">
<head>
    <?php
        $title = 'SiGi API Assist';
        switch ($_GET['option']) {
            case 'atividades_status':
                $title = 'SiGi API Assist | Status de Atividades';
                break;

            case 'atividades':
                $title = 'SiGi API Assist | Atividades';
                break;

            case 'locais':
                $title = 'SiGi API Assist | Locais';
                break;

            case 'contratacoes':
                $title = 'SiGi API Assist | Contratações';
                break;

            case 'contratos_status':
                $title = 'SiGi API Assist | Status de Contratos';
                break;

            case 'fornecedores_ramos':
                $title = 'SiGi API Assist | Ramos de Fornecedores';
                break;

            case 'fornecedores':
                $title = 'SiGi API Assist | Fornecedores';
                break;

            case 'aeic_divisoes':
                $title = 'SiGi API Assist | Divisões';
                break;

            case 'aeic_responsaveis':
                $title = 'SiGi API Assist | Responsávels dos Contratos';
                break;

            case 'aeic':
                $title = 'SiGi API Assist | A.E.I.C.s';
                break;
            
            default:
                $title = 'SiGi API Assist';
                break;
        }
    ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Lauro Damaceno">
    <meta name="description" content="Esta é uma ferramenta de documentação e extração dos dados do SiGi Legado">
    <link rel="icon" href="https://sigi2.netlify.app/assets/images/icon.png">
    <title><?= $title ?></title>

    <!-- Choices.JS -->
    <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/base.min.css"
    />
    <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css"
    />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/styles/monokai-sublime.min.css">
    <style>
        body {
            /*background-color: #2b2b2b;
            color: #f8f8f2;*/
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }
        #header {
            background-image: url("./assets/images/background.png");
            background-size: 100% auto;
            background-position: center center;
        }
        @media (max-width: 762px) {
            #header {
                background-size: auto 100%;
            }
        }
        .layer-blue {
            background-color: rgba(0, 0, 255, 0.49);
        }
        #rota {
            font-size: 2rem;
        }
        @media (max-width: 762px) {
            #rota {
                font-size: 1rem;
            }
        }
        #btn-copiar {
            cursor: pointer;
        }
        #notificacao-copy {
            position: fixed;
            bottom: 20px;
            left: 20px;
        }
        /*.choices {
            min-width: 30% !important;
        }*/
        textarea:focus, 
        textarea.form-control:focus, 
        input.form-control:focus, 
        input[type=text]:focus, 
        input[type=password]:focus, 
        input[type=email]:focus, 
        input[type=number]:focus, 
        [type=text].form-control:focus, 
        [type=password].form-control:focus, 
        [type=email].form-control:focus, 
        [type=tel].form-control:focus, 
        [contenteditable].form-control:focus {
            border: none;
            box-shadow: inset 0 -1px 0 #ddd;
        }
        #visual-tabela {
            overflow-x: hidden;
            white-space: nowrap;
            cursor: grab;
        }
        #tabela-dados {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
            height: 300px;
            overflow-y: scroll;
        }
        #tabela-dados thead th {
            position: sticky;
            top: 0px;
            z-index: 1
        }
        #tabela-dados td, 
        #tabela-dados th {
            border: 1px solid #999;
            text-align: left;
            padding: 8px;
        }
        #tabela-dados .remove-padding {
            border: none;
            padding: 0px;
        }
        #tabela-dados td {
            vertical-align: top;
            /*width: 100%;
            max-width: 320px;
            white-space: normal;
            overflow: hidden;*/
        }
        #tabela-dados th {
            text-transform: uppercase;
        }

        /*
        #tabela-dados tr:nth-child(even) {
            background-color: #dddddd;
        }*/

        .layout-tabela {
            width: 3500px;
            table-layout: fixed; /* Define o layout da tabela como fixo */
            border-collapse: collapse;
        }

        /*
        .layout-tabela .layout-tabela {
            width: 650px;
        }*/

        .divisor:not(:first-child) {
            border-top: #999 solid 15px !important;
        }

        .aeic .layout-tabela,
        .aeic td:nth-child(3),
        .aeic th:nth-child(3) {
            width: 650px;
        }

        .aeic .layout-tabela th,
        .aeic .layout-tabela td
        {
            width: 150px !important;
        }
 
        /*
        .layout-tabela .layout-tabela th:first-child,
        .layout-tabela .layout-tabela td:first-child {
            width: 100px;
        }*/

        .layout-tabela th:first-child,
        .layout-tabela td:first-child {
            width: 70px; /* Defina a largura desejada para a primeira coluna */
        }

        .layout-tabela th,
        .layout-tabela td {
            vertical-align: unset !important;
            white-space: normal;
            overflow: hidden
        }

        .tabela-fornecedores th:nth-child(11),
        .tabela-fornecedores td:nth-child(11),
        .tabela-atividades th:nth-child(11),
        .tabela-atividades td:nth-child(11),
        .tabela-atividades th:nth-child(13),
        .tabela-atividades td:nth-child(13),
        .tabela-atividades th:nth-child(15),
        .tabela-atividades td:nth-child(15),
        .tabela-atividades th:nth-child(16),
        .tabela-atividades td:nth-child(16),
        .obs-col {
            vertical-align: unset !important;
            white-space: normal;
            overflow: hidden;
            width: 400px;
            min-width: 400px;
        }

        #area-de-compartilhamento {
            position: fixed;
            right: 20px;
            bottom: 20px;
            display: flex;
            align-items: center;
        }

        #notificacao-erro {
            position: fixed;
            bottom: 20px;
            left: 20px;
            z-index: 99999;
        }
    </style>
    <link rel="stylesheet" href="./assets/css/tabela-requisicoes.css">
</head>
<body>
    <main>
        <header id="header" class="container-fluid">
            <div class="row">
                <div class="col-12 text-center py-5 p-md-5 text-white layer-blue" >
                    <div class="container-fluid">
                        <div class="row justify-content-center mt-md-5 mb-md-5">
                            <div class="col-12 col-md-6">
                                <h1 class="text-warning mb-md-3">Seja bem vindo(a) à<br/><strong>SiGi API LEGADO ASSIST</strong> <small>beta</small>!</h1>
                                <h4>Aqui você consegue extrair os dados de forma eficiente e futuramente, será capaz de conectar esta API à outras aplicações e soluções da Eletrobrás!</h4>
                            </div>
                        </div>
                        <input type="hidden" id="executar" <?= $_GET['executar'] == '1' ? 'value="1"' : 'value="0"' ?> onchange="handleSelectChange()" />
                        <section id="seletor-de-rota" class="row justify-content-center">
                            <div class="col-12 col-md-6 text-center mb-1 mb-md-0">
                                <select name="rota" id="rota" class="form-select w-100 text-center py-2 rounded-5" accesskey="A" onchange="handleSelectChange(1)">
                                    <option value="" <?= $_GET["option"] === '' ? 'selected' : '' ?>>O que você precisa?</option>
                                    <option value="atividades/status" <?= $_GET["option"] === 'atividades_status' ? 'selected' : '' ?>>Lista de status de atividade</option>
                                    <option value="atividades" <?= $_GET["option"] === 'atividades' ? 'selected' : '' ?>>Lista de atividades (SiGi)</option>
                                    <option value="locais" <?= $_GET["option"] === 'locais' ? 'selected' : '' ?>>Lista de locais</option>
                                    <option value="contratacoes" <?= $_GET["option"] === 'contratacoes' ? 'selected' : '' ?>>Lista de contratações</option>
                                    <option value="contratos/status" <?= $_GET["option"] === 'contratos_status' ? 'selected' : '' ?>>Lista de status contratos</option>
                                    <option value="contratos" <?= $_GET["option"] === 'contratos_status' ? 'selected' : '' ?>>Lista de contratos</option>
                                    <option value="fornecedores/ramos" <?= $_GET["option"] === 'fornecedores_ramos' ? 'selected' : '' ?>>Lista de ramos de fornecimento</option>
                                    <option value="fornecedores" <?= $_GET["option"] === 'fornecedores' ? 'selected' : '' ?>>Lista de fornecedores</option>
                                    <option value="aeic/divisoes" <?= $_GET["option"] === 'aeic_divisoes' ? 'selected' : '' ?>>Lista de divisões (A.E.I.C)</option>
                                    <option value="aeic/responsaveis" <?= $_GET["option"] === 'aeic_responsaveis' ? 'selected' : '' ?>>Lista de responsáveis (A.E.I.C)</option>
                                    <option value="aeic" <?= $_GET["option"] === 'aeic' ? 'selected' : '' ?>>Lista de A.E.I.C.s</option>
                                </select>
                            </div>
                        </section>
                        <section id="filtros" class="row justify-content-center mt-1 mt-md-3">
                            <div class="col-12 col-md-6 text-center d-md-flex">

                                <select id="conteudo" class="form-select text-center py-2 rounded-5 mb-2 mb-md-0 d-none" onchange="handleSelectChange()" >
                                    <option value="0" <?= $_GET["conteudo"] === '0' || $_GET["conteudo"] === ''  ? 'selected' : '' ?>>Exibir dividido em páginas</option>
                                    <option value="1" <?= $_GET["conteudo"] === '1' ? 'selected' : '' ?>>Mostrar tudo</option>
                                </select>

                                <input type="number" id="itens-por-pagina" <?= $_GET["itens-por-pagina"] === '' ? 'value="20"' : 'value="'.$_GET["itens-por-pagina"].'"' ?> class="form-control text-center py-2 rounded-5 mx-md-2 mb-2 mb-md-0 d-none" placeholder="Ítems por consulta (O Padrão é 20)" onchange="handleSelectChange()" />

                                <select id="pagina" <?= $_GET["pagina"] === '' ? 'value="1"' : 'value="'.$_GET["pagina"].'"' ?> class="form-select text-center py-2 rounded-5 mb-2 mb-md-0 d-none" onchange="handleSelectChange()" >
                                    <option value="1">Página 1</option>
                                </select>

                            </div>
                        </section>

                        <section id="filtros-atividades" class="row justify-content-center mt-1 d-none">
                            <div class="col-12 col-md-6 text-center d-md-flex">
                                <select id="local-atividade" <?= $_GET["local-atividade"] === '' ? 'value=""' : 'value="'.$_GET["local-atividade"].'"' ?> class="form-select text-center py-2 rounded-5 mb-2 me-md-1 mb-md-0" onchange="handleSelectChange()" >
                                    <option value="">Filtrar por local</option>
                                </select>
                                <select id="status-atividade" <?= $_GET["status-atividade"] === '' ? 'value=""' : 'value="'.$_GET["status-atividade"].'"' ?> class="form-select text-center py-2 rounded-5 mb-2 ms-md-1 mb-md-0" onchange="handleSelectChange()" >
                                    <option value="">Filtrar por status</option>
                                </select>
                            </div>
                        </section>

                        <section id="filtros-contratacoes" class="row justify-content-center mt-1 d-none">
                            <div class="col-12 col-md-6 text-center d-md-flex">
                                <input 
                                    type="text" 
                                    id="id-contratacao" 
                                    class="form-control text-center py-2 rounded-5 mx-md-2 mb-2 mb-md-0 me-md-1" 
                                    placeholder="Digite um trecho do nº da contratação"
                                    <?= $_GET['id-contratacao'] != '' ? 'value="'.$_GET['id-contratacao'].'"' : '' ?>
                                    onchange="handleSelectChange()" 
                                />
                                <input 
                                    type="text" 
                                    id="responsavel-contratacao" 
                                    class="form-control text-center py-2 rounded-5 mx-md-2 mb-2 mb-md-0 me-md-1" 
                                    placeholder="Pesquise pelo responsável da contratação"
                                    <?= $_GET['responsavel-contratacao'] != '' ? 'value="'.$_GET['responsavel-contratacao'].'"' : '' ?>
                                    onchange="handleSelectChange()" 
                                />
                            </div>
                        </section>

                        <section id="filtros-contratos" class="row justify-content-center mt-1 d-none">
                            <div class="col-12 col-md-6 text-center d-md-flex">
                                <select id="divisao-contrato" class="form-select text-center py-2 rounded-5 mb-2 mb-md-0" <?= $_GET['divisao-contrato'] != '' ? 'value="'.$_GET['divisao-contrato'].'"' : '' ?> onchange="handleSelectChange()" >
                                    <option value="">Filtrar por divisão</option>
                                </select>
                                <select id="status-contrato" class="form-select text-center py-2 rounded-5 mb-2 mx-md-2 mb-md-0" <?= $_GET['status-contrato'] != '' ? 'value="'.$_GET['status-contrato'].'"' : '' ?> onchange="handleSelectChange()" >
                                    <option value="">Filtrar por status</option>
                                </select>
                                <select id="responsavel-contrato" class="form-select text-center py-2 rounded-5 mb-2 mb-md-0" <?= $_GET['responsavel-contrato'] != '' ? 'value="'.$_GET['responsavel-contrato'].'"' : '' ?> onchange="handleSelectChange()" >
                                    <option value="">Filtrar por responsavel</option>
                                </select>
                            </div>
                        </section>

                        <section id="filtros-contratos-2" class="row justify-content-center mt-1 d-none">
                            <div class="col-12 col-md-6 text-center d-md-flex">
                                <select id="tipo-contrato" class="form-select text-center py-2 rounded-5 mb-2 mb-md-0 me-md-1" onchange="handleSelectChange()" >
                                    <option value="" <?= $_GET['tipo-contrato'] == '' ? 'selected' : '' ?>>Filtrar por tipo</option>
                                    <option value="1" <?= $_GET['tipo-contrato'] == '1' ? 'selected' : '' ?>>Contratos</option>
                                    <option value="2" <?= $_GET['tipo-contrato'] == '2' ? 'selected' : '' ?>>Licitações</option>
                                </select>
                                <input 
                                    type="text" 
                                    id="doc-contratual-contrato" 
                                    class="form-control text-center py-2 rounded-5 mb-2 mb-md-0 ms-md-1" 
                                    placeholder="Pesquise pelo doc contratual (nº SAP)"
                                    <?= $_GET['doc-contratual-contrato'] != '' ? 'value="'.$_GET['doc-contratual-contrato'].'"' : '' ?>
                                    onchange="handleSelectChange()" 
                                />
                            </div>
                        </section>

                        <section id="filtros-fornecedores" class="row justify-content-center mt-1 d-none">
                            <div class="col-12 col-md-6 text-center d-md-flex">
                                <select id="situacao-fornecedor" class="form-select text-center py-2 rounded-5 mb-2 mb-md-0" onchange="handleSelectChange()" >
                                    <option value="" <?= $_GET['situacao-fornecedor'] == '' ? 'selected' : '' ?>>Filtrar por situação de fornecedor</option>
                                    <option value="1" <?= $_GET['situacao-fornecedor'] == '1' ? 'selected' : '' ?>>Fornecedores <b>APTOS</b></option>
                                    <option value="0" <?= $_GET['situacao-fornecedor'] == '2' ? 'selected' : '' ?>>Fornecedores <b>INAPTOS</b></option>
                                </select>
                                <select id="porte-fornecedor" class="form-select text-center py-2 rounded-5 mb-2 ms-md-2 mb-md-0" onchange="handleSelectChange()" >
                                    <option value="" <?= $_GET['porte-fornecedor'] == '' ? 'selected' : '' ?>>Filtrar por porte do fornecedor</option>
                                    <option value="1" <?= $_GET['porte-fornecedor'] == '1' ? 'selected' : '' ?>>Pequena</option>
                                    <option value="2" <?= $_GET['porte-fornecedor'] == '2' ? 'selected' : '' ?>>Média</option>
                                    <option value="3" <?= $_GET['porte-fornecedor'] == '3' ? 'selected' : '' ?>>Grande</option>
                                </select>
                            </div>
                        </section>
                        <section id="filtros-fornecedores-2" class="row justify-content-center mt-1 d-none">
                            <div class="col-12 col-md-6 text-center d-md-flex">
                                <select id="ramo-fornecedor" multiple class="form-select text-center py-2 rounded-5 mb-2 mb-md-0 js-choice" <?= $_GET['ramo-fornecedor'] != '' ? 'value="'.$_GET['ramo-fornecedor'].'"' : '' ?> onchange="handleSelectChange()" >
                                    <option value="">Filtrar por ramo do fornecedor</option>
                                </select>
                            </div>
                        </section>

                        <section id="filtros-aeic" class="row justify-content-center mt-1 d-none">
                            <div class="col-12 col-md-6 text-center d-md-flex">
                                <select id="divisao-aeic" class="form-select text-center py-2 rounded-5 mb-2 mb-md-0" <?= $_GET['divisao-aeic'] != '' ? 'value="'.$_GET['divisao-aeic'].'"' : '' ?> onchange="handleSelectChange()" >
                                    <option value="">Filtrar por divisão</option>
                                </select>
                                <select id="status-aeic" class="form-select text-center py-2 rounded-5 mb-2 mx-md-2 mb-md-0" <?= $_GET['status-aeic'] != '' ? 'value="'.$_GET['status-aeic'].'"' : '' ?> onchange="handleSelectChange()" >
                                    <option value="">Filtrar por status</option>
                                </select>
                                <select id="responsavel-aeic" class="form-select text-center py-2 rounded-5 mb-2 mb-md-0" <?= $_GET['responsavel-aeic'] != '' ? 'value="'.$_GET['responsavel-aeic'].'"' : '' ?> onchange="handleSelectChange()" >
                                    <option value="">Filtrar por responsavel</option>
                                </select>
                            </div>
                        </section>

                        <section id="filtros-aeic-2" class="row justify-content-center mt-1 d-none">
                            <div class="col-12 col-md-6 text-center d-md-flex">
                                <select id="tipo-aeic" class="form-select text-center py-2 rounded-5 mb-2 mb-md-0 me-md-1" onchange="handleSelectChange()" >
                                    <option value="" <?= $_GET['tipo-aeic'] == '' ? 'selected' : '' ?>>Filtrar por tipo</option>
                                    <option value="1" <?= $_GET['tipo-aeic'] == '1' ? 'selected' : '' ?>>Contratos</option>
                                    <option value="2" <?= $_GET['tipo-aeic'] == '2' ? 'selected' : '' ?>>Licitações</option>
                                </select>
                                <input 
                                    type="text" 
                                    id="doc-contratual-aeic" 
                                    class="form-control text-center py-2 rounded-5 mb-2 mb-md-0 ms-md-1" 
                                    placeholder="Pesquise pelo doc contratual (nº SAP)"
                                    <?= $_GET['doc-contratual-aeic'] != '' ? 'value="'.$_GET['doc-contratual-aeic'].'"' : '' ?>
                                    onchange="handleSelectChange()" 
                                />
                            </div>
                        </section>

                        <section id="bt-send-filtros" class="row justify-content-center mt-3 d-none">
                            <div class="col-12 col-md-6 text-center d-md-flex">
                                <button class="btn btn-success w-100 py-3 h2 rounded-5" onClick="handleSubmit()">
                                    BUSCAR DADOS
                                </button>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
        </header>
        <section id="content" class="container-fluid mt-md-1 pt-md-3">
            <div class="row mt-3 mt-md-0">
                <div id="ferramentas" class="col-12 d-none">
                    <div class="container py-0">
                        <div class="row align-items-md-center">
                            <div id="rota-link" class="col-12 col-md-8">
                                <div class="input-group w-100">
                                    <span class="input-group-text bg-primary text-white" id="rota-topo"><strong>ROTA</strong></span>
                                    <input type="text" class="form-control rota-de-chamada m-0" placeholder="Rota..." aria-label="Rota" aria-describedby="rota" readonly>
                                    <span id="btn-copiar" class="input-group-text bg-primary text-white">Copiar</span>
                                </div>
                            </div>
                            <div class="col-12 col-md-2 mt-2 mt-md-0 mb-3 mb-md-0">
                                <input type="hidden" id="filename" class="form-control">
                                <button id="download-button-d" class="btn btn-primary w-100" onclick="downloadJson()">Baixar dados JSON</button>
                            </div>
                            <div class="col-12 col-md-2">
                                <button id="download-button-e" class="btn btn-success w-100" onclick="exportToExcelFromTable()">Baixar Excel</button>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div class="row pt-3 px-1">
                <div class="col-6">
                    <button class="w-100 btn btn-success h-100" onclick="Visualizacao({modo: 'tabela'})">
                        Ver em tabela
                    </button>
                </div>
                <div class="col-6">
                    <button class="w-100 btn btn-info text-white" onclick="Visualizacao({modo: 'dados'})">
                        Ver em formato de dados
                    </button>
                </div>
            </div>
            <div class="row">
                <div id="visual-dados" class="col-12 rounded-3 pt-3 px-3<?= $_GET["mode"] === 'dev' ? ' ' : ' d-none'; ?>">
                    <pre><code id="json-container" class="json"></code></pre>
                </div>
                <div id="visual-tabela" class="col-12 rounded-3 pt-3 px-3<?= $_GET["mode"] !== 'dev' ? ' ' : ' d-none'; ?>"></div>
            </div>
        </section>
        <footer class="container-fluid pt-3">
            <div class="row">
                <div class="col-12 text-center">
                    <p>Desenvolvido pela Equipe de T.I. e Inovação de Furnas 2023</p>
                </div>
            </div>
        </footer>

        <section>
            <div id="notificacao-copy" class="toast align-items-center text-white bg-success border-0" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="d-flex">
                    <div class="toast-body">
                        Rota copiada!
                    </div>
                    <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
            </div>

            <div id="notificacao-erro" class="toast align-items-center text-white bg-danger border-0" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="d-flex">
                    <div class="toast-body">
                        E-mail inválido
                    </div>
                    <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
            </div>

            <div id="area-de-compartilhamento" class="d-none">
                <!--<button id="share" data-bs-toggle="modal" data-bs-target="#share-busca" class="btn btn-success rounded-5 d-none">-->
                <button id="share" class="btn btn-success rounded-5 d-none">
                    Compartilhar busca
                </button>
                <!--<div
                    id="share-by-teams"
                    class="teams-share-button"
                    data-href=""
                    data-msg-text="Olá, tudo bem? Verifique estes dados do SiGi que estou lhe encaminhando por meio do abaixo:"
                    data-assign-title="Minha busca na ferramenta SiGi">
                </div>-->
            </div>
        </section>

        <div class="modal fade" id="share-busca" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="staticBackdropLabel">Compartilhe essa busca por e-mail</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body mb-0">
                    <input type="email" id="email-share" placeholder="Digite o e-mail aqui..." class="form-control mb-0" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
                    <button id="compartilhar-busca" type="button" class="btn btn-success">Enviar</button>
                </div>
                </div>
            </div>
        </div>
        
    </main>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/highlight.min.js"></script>
    <!-- Baixando arquivos em XLSX -->
    <script src="https://cdn.sheetjs.com/xlsx-0.20.0/package/dist/xlsx.full.min.js"></script>
    <!-- Compartilhamento via Teams -->
    <script async defer src="https://teams.microsoft.com/share/launcher.js"></script>
    <!-- Include Choices JavaScript (latest) -->
    <script src="https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js"></script>
    <script src="./assets/js/main.js"></script>
    <script src="./assets/js/access.js"></script>
    <script>
        const container = document.querySelector('#visual-tabela');
        let isDragging = false;
        let startX;
        let scrollLeft;

        // Função para iniciar o arrastar
        function startDragging(e) {
            isDragging = true;
            startX = e.pageX !== undefined ? e.pageX - container.offsetLeft : e.touches[0].pageX - container.offsetLeft;
            scrollLeft = container.scrollLeft;
            container.style.cursor = 'grabbing';
        }

        // Função para arrastar
        function drag(e) {
            if (!isDragging) return;
            e.preventDefault();
            const x = e.pageX !== undefined ? e.pageX - container.offsetLeft : e.touches[0].pageX - container.offsetLeft;
            const walk = (x - startX) * 2;
            container.scrollLeft = scrollLeft - walk;
        }

        // Função para parar o arrastar
        function stopDragging() {
            isDragging = false;
            container.style.cursor = 'grab';
        }

        // Eventos de mouse
        container.addEventListener('mousedown', startDragging);
        container.addEventListener('mousemove', drag);
        container.addEventListener('mouseup', stopDragging);
        container.addEventListener('mouseleave', stopDragging);

        // Eventos de toque
        container.addEventListener('touchstart', startDragging);
        container.addEventListener('touchmove', drag);
        container.addEventListener('touchend', stopDragging);
        container.addEventListener('touchcancel', stopDragging);
    </script>
</body>
</html>
